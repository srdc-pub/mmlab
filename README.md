## Building and Pushing the Docker Image

To build and push the custom Docker image to the GitLab container registry, follow these steps:

1. Open a terminal in the directory containing the Dockerfile.
2. Build the Docker image using the following command:

```sh
docker build -t registry.gitlab.com/srdc-pub/mmlab:t4 -f Dockerfile_t4 .
```

This command builds the Docker image using the provided Dockerfile (Dockerfile_t4) and tags it with the specified name and version (registry.gitlab.com/srdc-pub/mmlab:t4).


3. Push the built Docker image to the GitLab container registry using the following command:
```sh
docker push registry.gitlab.com/srdc-pub/mmlab:t4
```

This command uploads the Docker image to the GitLab container registry under the specified repository, making it available for use in other projects.

###Usage

To use the custom Docker image in your project, refer to the image using its full name and tag:

```yaml
image: registry.gitlab.com/srdc-pub/mmlab:t4
```
Replace registry.gitlab.com/srdc-pub/mmlab:t4 with the appropriate image name and tag based on your requirements.
